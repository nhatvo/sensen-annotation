from django.db import models


class LicensePlateDataset(models.Model):
    name = models.CharField(max_length=1024)
    description = models.TextField(default='')
    folder_path = models.CharField(
        max_length=1024,
        default='media/license_plate_images/your_folder_path'
    )
    is_active = models.BooleanField(default=True, blank=True, null=True)
    created_at = models.DateTimeField(auto_now_add=True)
    updated_at = models.DateTimeField(auto_now=True)

    def __str__(self):
        return self.name + '_' + str(self.pk)


class LicensePlate(models.Model):
    dataset = models.ForeignKey(LicensePlateDataset, on_delete=models.CASCADE)
    image = models.ImageField()
    ocr_read1 = models.CharField(max_length=20, default='', blank=True)
    ocr_read2 = models.CharField(max_length=20, default='', blank=True)
    ocr_read = models.CharField(max_length=20, default='', blank=True, db_index=True)
    created_at = models.DateTimeField(auto_now_add=True)
    updated_at = models.DateTimeField(auto_now=True)

    def __str__(self):
        return 'OCR: {}'.format(self.ocr_read)
