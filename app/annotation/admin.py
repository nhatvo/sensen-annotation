import csv

from django.contrib import admin
from django.http import HttpResponse
from django.utils.safestring import mark_safe
from google.cloud import storage

from .models import LicensePlate, LicensePlateDataset


def list_blobs_with_prefix(bucket_name, prefix, delimiter=None):
    """Lists all the blobs in the bucket that begin with the prefix.

    This can be used to list all blobs in a "folder", e.g. "public/".

    The delimiter argument can be used to restrict the results to only the
    "files" in the given "folder". Without the delimiter, the entire tree under
    the prefix is returned. For example, given these blobs:

        a/1.txt
        a/b/2.txt

    If you just specify prefix = 'a', you'll get back:

        a/1.txt
        a/b/2.txt

    However, if you specify prefix='a' and delimiter='/', you'll get back:

        a/1.txt

    Additionally, the same request will return blobs.prefixes populated with:

        a/b/
    """

    storage_client = storage.Client()

    # Note: Client.list_blobs requires at least package version 1.17.0.
    blobs = storage_client.list_blobs(
        bucket_name, prefix=prefix, delimiter=delimiter
    )

    path_list = []
    for blob in blobs:
        path_list.append(blob.name)

    if delimiter:
        print("Prefixes:")
        for prefix in blobs.prefixes:
            print(prefix)

    return path_list


class LicensePlateDatasetAdmin(admin.ModelAdmin):
    list_display = (
        'name',
        'folder_path',
        'description',
        'created_at'
    )

    def save_model(self, request, obj, form, change):
        obj.user = request.user
        super().save_model(request, obj, form, change)

        file_list = list_blobs_with_prefix(
            bucket_name='sensen-annotation',
            prefix=obj.folder_path
        )
        # check if this is a new dataset or just update detail
        if not obj.licenseplate_set.all().exists():
            for file_path in file_list:
                LicensePlate.objects.create(
                    dataset=obj,
                    image=file_path
                )


def export_as_csv_action(description="Export selected objects as CSV file", fields=None, exclude=None, header=True):
    """
    This function returns an export csv action
    'fields' and 'exclude' work like in django ModelForm
    'header' is whether or not to output the column names as the first row
    """
    def export_as_csv(modeladmin, request, queryset):
        """
        Generic csv export admin action.
        based on http://djangosnippets.org/snippets/1697/
        """
        opts = modeladmin.model._meta
        field_names = set([field.name for field in opts.fields])

        if fields:
            fieldset = set(fields)
            field_names = field_names & fieldset

        elif exclude:
            excludeset = set(exclude)
            field_names = field_names - excludeset

        response = HttpResponse(content_type='text/csv')
        response['Content-Disposition'] = 'attachment; filename=%s.csv' % str(
            opts).replace('.', '_')

        writer = csv.writer(response)

        field_names = list(field_names)
        field_names.sort()
        if header:
            writer.writerow(field_names)
        for obj in queryset:
            writer.writerow([str(getattr(obj, field)) for field in field_names])

        return response

    export_as_csv.short_description = description
    return export_as_csv


class LicensePlateAdmin(admin.ModelAdmin):
    def get_queryset(self, request):
        qs = super().get_queryset(request)
        return qs.filter(dataset__is_active=True)

    def get_list_display(self, request):
        default_list_display = super(
            LicensePlateAdmin, self).get_list_display(request)
        if request.user.is_superuser:
            default_list_display = [
                'get_plate_image',
                'ocr_read',
                'ocr_read1',
                'ocr_read2',
                'created_at',
                'updated_at'
            ]

        if 'team1' in request.user.username:
            default_list_display = [
                'get_plate_image',
                'ocr_read1',
                'created_at',
                'updated_at'
            ]

        elif 'team2' in request.user.username:
            default_list_display = [
                'get_plate_image',
                'ocr_read2',
                'created_at',
                'updated_at'
            ]
        return default_list_display

    def get_list_editable(self, request):
        """
        get_list_editable method implementation, 
        django ModelAdmin doesn't provide it.
        """
        if request.user.is_superuser:
            dynamically_editable_fields = ('ocr_read1', 'ocr_read2',)
        if 'team1' in request.user.username:
            dynamically_editable_fields = ('ocr_read1',)
        elif 'team2' in request.user.username:
            dynamically_editable_fields = ('ocr_read2',)
        return dynamically_editable_fields

    def get_search_fields(self, request):
        """
        get_search_fields method implementation, 
        """
        if request.user.is_superuser:
            dynamically_search_fields = ('ocr_read', 'ocr_read1', 'ocr_read2',)
        if 'team1' in request.user.username:
            dynamically_search_fields = ('ocr_read1',)
        elif 'team2' in request.user.username:
            dynamically_search_fields = ('ocr_read2',)
        return dynamically_search_fields

    def get_changelist_instance(self, request):
        """
        override admin method and list_editable property value 
        with values returned by our custom method implementation.
        """
        self.list_editable = self.get_list_editable(request)
        return super(LicensePlateAdmin, self).get_changelist_instance(request)

    def save_model(self, request, obj, form, change):
        obj.user = request.user
        ocr_read1 = obj.ocr_read1.upper().strip()
        ocr_read2 = obj.ocr_read2.upper().strip()
        obj.ocr_read1 = ocr_read1
        obj.ocr_read2 = ocr_read2
        if len(ocr_read1) > 0 and ocr_read1 == ocr_read2:
            obj.ocr_read = ocr_read1

        obj.save()

        super().save_model(request, obj, form, change)

    list_per_page = 10
    list_filter = ['dataset__name']

    actions = [
        export_as_csv_action(
            "CSV Export",
            fields=['image', 'ocr_read', 'ocr_read1', 'ocr_read2']
        )
    ]

    def get_plate_image(self, obj):
        return mark_safe('<img src="%s"/>' % (obj.image.url))

    get_plate_image.short_description = 'Image'


admin.site.register(LicensePlateDataset, LicensePlateDatasetAdmin)
admin.site.register(LicensePlate, LicensePlateAdmin)
