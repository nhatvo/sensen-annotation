#!/bin/sh

# It's better to run these command manually
# python manage.py collectstatic --noinput
# python manage.py flush --no-input
# python manage.py migrate

# Create superuser if not exists
echo "from django.contrib.auth.models \
import User; \
User.objects.filter(username='sensen').exists() or \
User.objects.create_superuser('sensen', 'admin@localhost', 'testsensen')" | \
python manage.py shell

echo "from django.contrib.auth.models \
import User; \
User.objects.filter(username='sensen_team1').exists() or \
User.objects.create_superuser('sensen_team1', 'sensen_team1@localhost', 'testsensen')" | \
python manage.py shell

echo "from django.contrib.auth.models \
import User; \
User.objects.filter(username='sensen_team2').exists() or \
User.objects.create_superuser('sensen_team2', 'sensen_team2@localhost', 'testsensen')" | \
python manage.py shell

exec "$@"